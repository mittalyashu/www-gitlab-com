require 'active_support/cache'
require 'active_support/core_ext/date/calculations'
require 'active_support/core_ext/numeric/time'
require 'active_support/notifications'

require 'faraday_middleware'
require 'faraday_middleware/parse_oj'

module GitlabParamsEncoder
  extend self

  def encode(hash)
    # The GitLab API does _not_ handle encoded `+` properly in label names
    Faraday::NestedParamsEncoder.encode(hash).gsub('%2B', '+')
  end

  def decode(string)
    Faraday::NestedParamsEncoder.decode(string)
  end
end

class GitLabInstance
  attr_reader :name

  def initialize(name)
    raise 'PRIVATE_TOKEN required to generate direction page' unless ENV['PRIVATE_TOKEN']

    @connection = Faraday.new(
      url: endpoint,
      headers: headers,
      request: { params_encoder: GitlabParamsEncoder, timeout: 90 }
    ) do |config|
      config.request :json
      config.response :logger, nil, { headers: false, bodies: false }
      config.response :oj
      config.adapter Faraday.default_adapter
    end

    @name = name
  end

  def get(path, params = {})
    cache_key = @connection.build_url(path, params).to_s

    response = cache_store.read(cache_key)

    # Unless the cached response exists and was successful, perform the request
    if response.nil? || response.status != 200
      begin
        response = @connection.get(path, params)

        if response.status == 200
          cache_store.write(cache_key, response)

          if response.headers['X-Total-Pages'].to_i > 1
            warn "WARNING: More than one page returned by #{response.env.url}"
          end
        else
          warn "Error in retrieving URL #{response.env.url}: #{response.status}"

          return []
        end
      rescue Faraday::ClientError => ex
        warn "Error in retrieving URL #{ex.inspect}"

        return []
      end
    end

    response.body
  end

  private

  def endpoint
    'https://gitlab.com/api/v4'
  end

  def headers
    {
      'PRIVATE-TOKEN' => ENV['PRIVATE_TOKEN'],
      'User-Agent' => 'www-gitlab-com'
    }
  end

  def cache_store
    @cache_store ||= ActiveSupport::Cache::FileStore.new(
      File.expand_path('../tmp/cache/direction', __dir__),
      expires_in: 36.hours.to_i
    )
  end
end

class GitLabProject
  def initialize(id, instance)
    @id = id.gsub('/', '%2F')
    @instance = instance
    @name = instance.name
  end

  def milestones
    result = @instance.get("projects/#{@id}/milestones", state: 'active')
    result = result.select { |ms| ms['due_date'] }
    result.sort_by! do |ms|
      Date.parse ms['due_date']
    end
  end

  def milestone(milestone_id)
    @instance.get("projects/#{@id}/milestones/#{milestone_id}")
  end

  def milestone_direction_issues(milestone_id)
    @instance.get("projects/#{@id}/issues", milestone: milestone_id, labels: 'direction')
  end

  def wishlist_issues(label, not_label = nil)
    result = @instance.get("projects/#{@id}/issues", labels: "direction,#{label}", state: 'opened', per_page: 100, sort: 'asc')
    result = result.select { |issue| (issue['labels'] & not_label).empty? } if not_label
    result.select { |issue| issue['milestone'].nil? || issue['milestone']['title'] == 'Backlog' }
  end

  def product_vision_issues(label, not_label = nil)
    result = @instance.get("projects/#{@id}/issues", labels: "Product+Vision+2018,#{label}", per_page: 100, sort: 'asc')
    result = result.select { |issue| (issue['labels'] & not_label).empty? } if not_label
    result
  end

  def tier_issues(tier)
    @instance.get("projects/#{@id}/issues", labels: "direction,#{tier}", state: 'opened', per_page: 100, sort: 'asc')
    # result = result.select { |issue| issue["milestone"] && issue["milestone"]["due_date"] }
  end

  def name
    project['name']
  end

  def web_url
    project['web_url']
  end

  def project
    @project ||= @instance.get("projects/#{@id}")
  end
end

class GitLabGroup
  def initialize(id, instance)
    @id = id
    @instance = instance
    @name = instance.name
  end

  def milestones
    result = @instance.get("groups/#{@id}/milestones", state: "active")
    result = result.select { |ms| ms['due_date'] }
    result.sort_by! do |ms|
      Date.parse ms['due_date']
    end
  end

  def product_vision_epics(label, not_label = nil)
    result = @instance.get("groups/#{@id}/epics", labels: "Product+Vision+2018,#{label}", per_page: 100, sort: 'asc')
    result = result.select { |epic| (epic['labels'] & not_label).empty? } if not_label
    result
  end

  def group
    @group ||= @instance.get("groups/#{@id}")
  end
end

def issue_bullet(issue)
  output = "- [#{issue['title']}](#{issue['web_url']})"
  output << ' <kbd>Starter</kbd>' if issue['labels'].include? 'GitLab Starter'
  output << ' <kbd>Premium</kbd>' if issue['labels'].include? 'GitLab Premium'
  output << ' <kbd>Ultimate</kbd>' if issue['labels'].include? 'GitLab Ultimate'
  output << "\n"
  output
end

def tier_bullet(issue)
  output = "- [#{issue['title']}](#{issue['web_url']})"
  output << " <kbd>#{issue['milestone']['title']}</kbd>" if issue['milestone']
  output << "\n"
  output
end

def product_vision_bullet(issue)
  output = "<i class=\"vision-item fa fa-#{'check-' if issue['state'] == 'closed'}square-o\" aria-hidden=\"true\"></i> [#{issue['title']}](#{issue['web_url']})"
  output << ' <kbd>Starter</kbd>' if issue['labels'].include? 'GitLab Starter'
  output << ' <kbd>Premium</kbd>' if issue['labels'].include? 'GitLab Premium'
  output << ' <kbd>Ultimate</kbd>' if issue['labels'].include? 'GitLab Ultimate'
  output << "<br>\n"
  output
end

def epic_web_url(group, epic)
  "#{group.group['web_url']}/-/epics/#{epic['iid']}"
end

def product_vision_bullet_from_epic(group, epic)
  output = "<i class=\"vision-item fa fa-minus-square-o\" aria-hidden=\"true\"></i> [#{epic['title']}](#{epic_web_url(group, epic)})"
  output << ' <kbd>Starter</kbd>' if epic['labels'].include? 'GitLab Starter'
  output << ' <kbd>Premium</kbd>' if epic['labels'].include? 'GitLab Premium'
  output << ' <kbd>Ultimate</kbd>' if epic['labels'].include? 'GitLab Ultimate'
  output << "<br>\n"
  output
end

def generate_direction
  puts 'Generating direction...'
  direction_output = ''

  milestones = gitlaborg.milestones
  milestones.sort_by! do |ms|
    Date.parse ms['due_date']
  end
  milestones.each do |ms|
    next unless ms['due_date'] && Date.parse(ms['due_date']) >= Date.today
    issues = []
    edition.each do |project|
      issues += project.milestone_direction_issues(ms['title'])
    end
    next if issues.empty?
    direction_output << "#### #{ms['title']} \n\n"
    issues.each do |issue|
      direction_output << issue_bullet(issue)
    end
    direction_output << "\n"
  end

  puts

  direction_output
end

def edition
  @edition ||= begin
                 com = GitLabInstance.new('GitLab.com')
                 ce = GitLabProject.new('gitlab-org/gitlab-ce', com)
                 ee = GitLabProject.new('gitlab-org/gitlab-ee', com)
                 omnibus = GitLabProject.new('gitlab-org/omnibus-gitlab', com)
                 [ce, ee, omnibus]
               end
end

def gitlaborg
  @group ||= begin
               com = GitLabInstance.new('GitLab.com')
               GitLabGroup.new('gitlab-org', com)
             end
end

def label_list(label, exclude: nil, editions: nil)
  output = ''

  editions = edition if editions.nil?

  editions.each do |project|
    issues = project.wishlist_issues(label, exclude)
    issues.each do |issue|
      output << issue_bullet(issue)
    end
  end
  output = "No current issues\n" if output.empty?
  output
end

def tier_list(label)
  output = ''
  issues = []

  edition.each do |project|
    issues += project.tier_issues(label)
  end
  issues.sort_by! do |issue|
    if issue.dig('milestone', 'due_date')
      Date.parse(issue['milestone']['due_date'])
    else
      Date.new(2050, 1, 1)
    end
  end
  issues.each do |issue|
    output << tier_bullet(issue)
  end

  output = "No current issues\n" if output.empty?
  output
end

def product_vision_list(label, not_label = nil, editions = nil)
  output = ''

  epics = gitlaborg.product_vision_epics(label, not_label)
  epics.each do |epic|
    output << product_vision_bullet_from_epic(gitlaborg, epic)
  end

  editions = edition if editions.nil?

  editions.each do |project|
    issues = project.product_vision_issues(label, not_label)
    issues.each do |issue|
      output << product_vision_bullet(issue)
    end
  end
  output = "No issues\n" if output.empty?
  output
end

def generate_wishlist
  puts 'Generating wishlist...'
  output = {}

  [
    'chat commands',
    'ci-build',
    'code review',
    'container registry',
    'deploy',
    'deliver',
    'issue boards',
    'issues',
    'pages',
    'pipeline',
    'major wins',
    'moderation',
    'moonshots',
    'open source',
    'performance',
    'Monitoring',
    'service desk',
    'test',
    'usability',
    'vcs for everything',
    'wiki',
    'HA',
    'Cloud Native'
  ].each do |label|
    output[label] = label_list(label)
  end
  output['CI/CD'] = label_list('CI/CD', exclude: ['ci-build', 'deploy', 'deliver', 'pages', 'pipeline', 'test', 'container registry', 'chat commands'])
  output['build'] = label_list('direction', exclude: ['HA', 'Cloud Native'], editions: Array(edition[2]))

  ['GitLab Starter', 'GitLab Premium', 'GitLab Ultimate'].each do |tier|
    output[tier] = tier_list(tier)
  end

  puts

  output
end

def generate_product_vision
  puts 'Generating product vision...'
  output = {}

  [
    'project management',
    'portfolio management',
    'repository',
    'code review',
    'web ide',
    'license management',
    'devops:package',
    'devops:release',
    'application control panel',
    'infrastructure configuration',
    'operations',
    'feature management',
    'application performance monitoring',
    'infrastructure monitoring',
    'production monitoring',
    'error tracking',
    'logging',
    'performance',
    'Omnibus',
    'Cloud Native'
  ].each do |label|
    output[label] = product_vision_list(label)
  end
  output['ci'] = product_vision_list('devops:verify', ['Security Products'])
  output['security testing'] = product_vision_list('devops:secure', ['license management'])

  puts

  output
end

if $PROGRAM_NAME == __FILE__
  generate_direction
  generate_wishlist
  generate_product_vision
end
