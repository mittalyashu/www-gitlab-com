---
layout: markdown_page
title: "2018 Q3 OKRs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV according to plan. IACV at 120% of plan, pipeline for Q4 at 3x IACV minus in quarter orders, LTV/CAC per campaign and type of customer.

* VPE
  * Support
    * Self-hosted: 95% on CSAT, 95% on Premium SLAs
    * Services: 95% on CSAT, 95% on Premium SLAs

### CEO: Popular next generation product. Complete DevOps GA, GitLab.com ready for mission critical applications, graph of DevOps score vs. cycle time.

* VPE
  * Frontend: Implement 10 performance improvements: X/10 (X%)
  * Dev Backend
    * Discussion: Implement 10 [performance improvements](https://gitlab.com/groups/gitlab-org/-/epics/247): X/10 (X%)
    * Distribution: Implement 10 performance improvements: X/10 (X%)
    * Geo: Implement 5 performance improvements (P1/P2/P3 categories): X/5 (X%)
    * Gitaly: Ship v1.0 and turn off NFS
    * Platform: Implement 10 [performance improvements](https://gitlab.com/groups/gitlab-org/-/epics/246): X/10 (X%)
    * Gitter: Open source Android and iOS applications
    * Gitter: Deprecate Gitter Topics
  * Ops Backend
    * CI/CD: Implement one key architectural performance improvement from https://gitlab.com/gitlab-org/gitlab-ce/issues/46499,
    * Configuration: API first Feature Flags: ship a feature flags MVC, all frontend code interacts via API to server, no server side rendering for feature flags
    * Monitoring: Implement admin performance dashboad. (Design issue TBD)
    * Security Products: Aggregate and store vulnerabilities occurences into database
      ([&241](https://gitlab.com/groups/gitlab-org/-/epics/241))
* VPE: Make GitLab.com ready for mission-critical customer workloads (99.95% availability)
  * Frontend: 100% uptime according to TBD budget
  * Dev Backend
    * Discussion: 100% uptime according to TBD budget
    * Distribution: 100% uptime according to TBD budget
    * Geo
       * Finish failover to Google Cloud
       * 100% uptime according to TBD budget
    * Gitaly: 100% uptime according to TBD budget
    * Gitter: 100% uptime according to TBD budget
    * Platform: 100% uptime according to TBD budget
  * Infrastructure
    * Database: 100% uptime according to TBD budget
    * Production: 100% uptime according to TBD budget
  * Ops Backend
    * CI/CD: 100% uptime according to https://gitlab.com/gitlab-com/infrastructure/issues/4450,
    * Configuration: 100% uptime according to TBD budget
    * Monitoring: 100% uptime according to TBD budget
    * Security Products: 100% uptime according to TBD budget
  * Quality
    * Implement Review Apps for CE/EE with gitlab-qa test automation validation as part of every Merge Request CI pipeline.
    * Triage and add proper priority and severity labels to 80% CE/EE bugs with no priority and severity labels.    
  * UX
    * Establish the User Experience direction for the security dashboard: Aid in completing a Competitive analysis, identify 10 must have items for the security dashboard, complete a design artifact/discovery issue.
    * UX Research
      * Incorporate personas into our design process for 100% of our product features.
      * Identify 5 pain points for users who have left GitLab.com. Work with Product Managers to identify solutions.
  * Security
    * Identify and push top 10 sources/attributes to S3 instance and ensure at least 90 day retention and tooling for security investigations.
    * Triage 50 backlogged security labeled issues and add appropriate severity and priority labels.    

### CEO: Great team. ELO score per interviewer, Employer branding person, Real-time dashboard for all Key Results.

* VPE: 10 iterations to engineering function handbook: X/10 (X%)
  * Frontend: 10 iterations to frontend department handbook: X/10 (X%)
  * Infrastructure: 10 iterations to infrastructure department handbook: X/10 (X%)
    * Production: 10 iterations to production team handbook: X/10 (X%)
  * Ops Backend: 10 iterations to ops backend department handbook: X/10 (X%)
    * CI/CD: 10 iterations to CI/CD team handbook: X/10 (X%)
    * Monitoring: 10 iterations to monitoring team handbook: X/10 (X%)
  * Quality: 10 iterations to quality department handbook: X/10 (X%)
  * Security: 10 iterations to security department handbook: X/10 (X%)
  * Support: 10 iterations to support department handbook: X/10 (X%)
      * Self-hosted: 10 iterations to self-hosted team handbook: X/10 (X%)
      * Services: 10 iterations to services team handbook: X/10 (X%)
* VPE: Source 300 candidates for various roles: X sourced (X%)
  * Frontend: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
  * Dev Backend: Create and apply informative template for team handbook pages
    across department
  * Dev Backend: Create well-defined hiring process for ICs and managers
    documented in handbook, ATS, and GitLab projects
  * Dev Backend: Source 20 candidates by July 15 and hire 2: 0 sourced (0%), hired 0 (0%)
    * Discussion: Source 50 candidates by July 15 and hire 2: X sourced (X%), hired X (X%)
    * Distribution: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Gitaly: Source 30 candidates by July 15 and hire 3: 0 sourced (0%), hired 0 (0%)
    * Platform: Source 75 candidates by July 15 and hire 3: X sourced (X%), hired X (X%)
  * Infrastructure: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Database: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Production: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
  * Ops Backend: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * CI/CD: Source 60 candidates by July 15 and hire 2: X sourced (X%), hired X (X%)
    * Configuration: Source 60 candidates by July 15 and hire 2: 0 sourced (0%), hired 0 (0%)
    * Monitoring: Source 100 candidates by July 15 and hire 2: X sourced (X%), hired X (X%)
    * Security Products: Source 50 candidates by July 15 and hire 2: 0 sourced (0%), hired 0 (0%)
  * Quality: Source 100 candidates by July 15 and hire 2: X sourced (X%), hired X (X%)
  * Security: Source 100 candidates by July 15 and hire 3: X sourced (X%), hired X (X%)
  * Support: Source 100 candidates by July 15 and hire EMEA and APAC managers: X sourced (X%), hired X (X%)
    * Self-hosted: Source 350 candidates by July 30 and hire 7: X sourced (X%), hired X (X%)
    * Services: Source 200 candidates by July 30 and hire 4: X sourced (X%), hired X (X%)
  * UX: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
* CFO: Great team
  * Controller: Analysis and proposal on trinet conversion including cost/benefit analysis of making the move (support from peopleops)
  * Controller: Transition expense reporting approval process to payroll and payments lead.
  * Controller: Full documentation of payroll process (sox compliant)
