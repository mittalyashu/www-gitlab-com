---
layout: job_page
title: "Data Analyst"
---

## Responsibilities

* Ensure the Company’s cloud and on-premise data is centralized into a single data warehouse that can support data analysis requirements from all functional groups of the Company. See our [bizops effort](https://gitlab.com/bizops/bizops/).
* Be the data expert supporting cross-functional teams, gather data from various sources, and build automated reporting to democratize data across the company.
* Provide analyses to identify opportunities and explain trends
* Implement a set of processes that that ensure any changes in transactional system architecture are documented and their impact on the company’s overall data integrity are considered prior to changes being made.
* Build close partnerships with functional owners to help them optimize their resources for allocation, and work with cross-functional teams to help them use insights that will direct their efforts towards the largest impact.
* Create a common data framework so that all company data can be analyzed in a unified manner.
* This position reports to the Manager, Data & Analytics. 



## Requirements

* 2+ years hands on experience in a data analytics role
* 2+ years experience with Python analytics tools suites (pandas, numpy, Jupyter notebooks, etc.).
* Demonstrably deep understanding of SQL and relational databases (Postgres preferred).
* Hands on experience working with with Python, R, and/or SQL to generate business insights and drive better organizational decision making.
* Experience building reports and dashboards in a data visualization tool like Tableau, Birst, or Looker 
* Be passionate about data, analytics and automation.
* Experience working with large quantities of raw, disorganized data
* Experience with Salesforce, Zuora, Zendesk, and Marketo as data sources as well as product usage data. 
* Share and work in accordance with our values
* Must be able to work in alignment with Americas timezones
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).


## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Manager, Data & Analytics
* Next, candidates will be invited to schedule a second interview with our Finance Operations and Planning Lead
* Candidates will then be invited to schedule a third interview with our CFO
* Finally, candidates may be asked to interview with our CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).
