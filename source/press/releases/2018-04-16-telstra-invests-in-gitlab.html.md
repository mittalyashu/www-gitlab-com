---
layout: markdown_page
title: "Telstra Ventures invests in GitLab to boost innovation and collaboration"
---

## GitLab's open core DevOps application allows enterprises to speed up product development and delivery to market
 
SAN FRANCISCO (April 16, 2018) — GitLab, the only software that supports the entire DevOps lifecycle in a single application, today announced that Telstra Ventures has invested in the company and will leverage its application to support innovation and optimise collaborative workflows.
 
Telstra Ventures, the investment arm of Australia’s leading telecommunications and technology company, chose to invest in GitLab for its open core DevOps philosophy that supports the entire development and operations lifecycle, and its capability to enable software development teams to achieve faster DevOps lifecycles.
 
“Customers are increasingly demanding better digital experiences, and DevOps is becoming the leading way for companies to develop, deliver and support applications that drive great customer experiences,” said Mark Sherman, Managing Director at Telstra Ventures. “One of the reasons we decided to invest is because GitLab is committed to continuously improving its application, which is key to helping companies rapidly take their best ideas from development to market.”
 
GitLab empowers companies to develop new solutions and capabilities more quickly by providing an end-to-end DevOps solution in a single application, while also providing the flexibility to plug into many developer tools such as JIRA and Jenkins. The software’s collaborative approach gives development, quality assurance, security and operations teams the ability to concurrently work on the same project within a single application and to see the entire workflow from their own point of view. GitLab is now trusted by more than 100,000 organizations worldwide, including Expedia, Sony, Intel, Red Hat, and NASDAQ.
 
“We look forward to partnering with Telstra to support its large application team and to aid the company in its vision of connecting people through technology,” said Sid Sijbrandij, CEO and co-founder of GitLab. “DevOps is increasingly being adopted by organizations around the globe to radically improve productivity and the pace at which software moves from idea to market.”
 
Many companies are realizing the importance of driving increased collaboration among their teams for successful adoption of DevOps to support the entirety of the software development lifecycle. GitLab’s recent [global survey](/developer-survey/2018/) of 5,296 software developers, CTOs and IT professionals found that 55 percent of respondents are still using at least 5 tools for their development processes and 62 percent of the respondents acknowledged losing time due to context switching between tools on a typical work day. Though 94 percent of respondents said it was important for them to work in a collaborative environment, visibility and transparency continues to lag with half of the developers. As the only single software application that supports the entire DevOps lifecycle, GitLab’s application is built from the ground up to enable collaboration amongst teams adopting DevOps, in companies ranging from Fortune Global 500 enterprises to fast-paced startups.

**About GitLab**

GitLab is a single application built from the ground up for all stages of the DevOps lifecycle for Product, Development, QA, Security, and Operations teams to work concurrently on the same project. GitLab provides teams a single data store, one user interface, and one permission model across the DevOps lifecycle, allowing teams to collaborate and work on a project from a single conversation, significantly reducing cycle time, and focus exclusively on building great software quickly. Built on Open Source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations from startups to global enterprise organizations, including Ticketmaster, ING, NASDAQ, Alibaba, Sony, and Intel trust GitLab to deliver great software at new speeds.

**Media Contact**

Nicole Plati

gitlab@highwirepr.com

415-963-4174 ext. 39