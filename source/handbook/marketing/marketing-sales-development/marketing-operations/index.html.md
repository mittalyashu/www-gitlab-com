---
layout: markdown_page
title: "Marketing Operations"
---


## On this page
{:.no_toc}

- TOC
{:toc}

## Marketing Operations

Marketing Operations (Mktg OPS) supports the entire Marketing team as well as other teams within GitLab. Mktg OPS works closely with Sales Operations to ensure information between systems is seamless and we are using consistent terminology in respective systems. 	


## Tech Stack  

For information regarding the tech stack at GitLab, please visit the [Business Operations section](/handbook/business-ops#tech-stack) of the handbook where we maintain a comprehensive table of the tools used across the Marketing, Sales, and Customer Success functional groups.  

## How-tos & FAQs

### Marketing Team OKR Issue Board

To track our Marketing functional group OKRs, we use a team [Issue Board](https://gitlab.com/gitlab-com/marketing/boards/444717?milestone_title=Q1%202018&=).   

To create an OKR issue do the following:   
 - **Title**: [META] {Team Name} OKR 1 (2 or 3): {Name of your OKR}  
      - Do not use a hash in front of the OKR number, it will associate the issue with existing issue with same number. Incorrect: '#1'
      - If using a hash in front of the OKR number put a space between the two characters. Correct: '# 1'
 - **Description**: List your 3 Key Results as todos/checkboxes  
 - **Assignee**: Assign to yourself   
 - **Milestone**: Associate the [META] Issue with the appropriate quarterly milestone (e.g. 'Q4 2017')
 - **Labels**: 'OKR', your team label (Design, Content, etc), and appropriate status - 'Not Started' (starting position for 99% of OKRs), 'Progressing', or 'Partially Completed'   

When you start working on an OKR, update the label from ‘Not Started’ -> ‘Progressing’. If creating "sub-Issues" related to your META OKR Issue, associate using the 'Related Issue' section.

Merge Requests related to an OKRs should also be associated with the appropriate quarterly Milestone and contain ‘OKR’ & your team label.

### Webcast &amp; Event Setup   
When creating an event (live or online), webcast, or gated content its important to integrate the campaign across the three main platforms used - Unbounce (landing page), Marketo (Marketing Automation) and Salesforce (CRM). This provides transparency about the event to anyone with access to Salesforce, namely the Sales and Marketing teams.  

Instructions to create an event from scratch or clone an existing event can be found on the google drive by searching for "Creating Events/Webcasts Process".

## Marketing Expense Tracking

| GL Code | Account Name | Purpose |
| :--- | :--- | :--- |
| 6100 | Marketing|Reserved for Marketing GL accounts|
| 6110 | Marketing Site|All software subscriptions, agency fees and contract work intended to improve the marketing site |
| 6120 | Advertising|All media buying costs as well as agency fees and software subscriptions related to media buying |
| 6130 | Events|All event sponsorships, booth shipping, event travel, booth design, event production as well as agency fees and software costs related to events |
| 6140 | Email|All 3rd party email sponsorships as well as agency fees and software costs related to mass email communications and marketing automation |
| 6150 | Brand|All PR, AR, content, swag and branding costs |
| 6160 | Prospecting|All costs related to prospecting efforts |

## Campaign Cost Tracking
Tracking costs associated with specific campaigns can be achieved by working with the Finance team to create custom tags. These tags can be applied to Expensify reports, corporate credit card charges, and vendor bills processed by Accounts Payable. Campaign expenses that are incurred by independent contractors should be clearly noted and included in their invoices to the company. Follow the steps below to create and use campaign tags.

1. Send an email request to the Accounts Payable that includes the campaign name.
2. Finance will open the tag in NetSuite and Expensify, notifying the requester when the tag is live.
3. Once the tag is ready to use, update the related issue to notify all stakeholders of the tag and its parameters. All costs including attendee travel expenses must be tagged in order to capture the true cost of campaigns. 
    * Tagging expenses that are processed by Accounts Payable require Marketing to provide explicit instruction on when to apply tags, which should happen during the normal course of reviewing and approving vendor invoices. For expenses that are not campaign related, include a note to Accounts Payable clearly stating that campaigns are not applicable to the expense. This is required to ensure all campaign expenses are tagged.


## Email Management

Email database management is a core responsibility for Mktg OPS. Ensuring GitLab is following email best practices, in compliance with International spam laws and overall health of active database are all priorities.  

### Requesting an Email

To request an email, create an issue in Marketing and assign it to JJ Cordz (in addition to whomever else needs to be aware of email). Required request information, please be as complete as possible in providing detail related to the email:
- **Sender Name**: Typically we use GitLab Team for most outgoing communications; for Security Alerts we use GitLab Security. Choosing a name that is consistent with the type and/or content of email being sent is important, if unsure make a note and we will make recommendation  
- **Sender Email Address**: What email address should be used?
- **Subject Line**: 50 character max is preferred
- **Email Body Copy**: Can be a text snippet within issue, clearly identified comment on issue or attach a Google doc with copy
- **Target Date to Send Email**: at a minimum a few days notice is preferred because we need to balancing the number of emails being sent to our database so they are not perceived (or marked) as spam; however, a simple email can be turned in a few hours if absolutely necessary
- **Recipient List**: Emails can be sent to one of the [existing segments](/handbook/marketing/marketing-sales-development/marketing-operations//#email-segments) or a recipient list can be provided as a .csv file

### Email Communication Policy  

As of 22 August 2017, all email marketing communications are explicit opt-in. Using the [Email Subscription Center](https://page.gitlab.com/SubscriptionCenter.html), users can control their email communication preferences. There are currently 4 [email segments](/handbook/marketing/marketing-sales-development/marketing-operations//#email-segments).

### Email Segments

Database segments and how someone subscribes to specific segment:  

- **Newsletter**: Users can subscribe to the newsletter through the blog, Contact Us page, and CE download page.
- **Security Alerts**: Subscribe on the GitLab [Contact Us page](/contact/)
- **Webcasts**: When someone registers to a live or on-demand webcast
- **Live Events**: When someone registers to attend a live event, meet up or in-person training. Use of this segment is narrowed down by geolocation so notification and invitation emails are specific to related area.  

### Types of Email

**Breaking Change Emails**  
These are operation emails that can be sent on a very selective as needed basis. This is an operational-type email that overrides the unsubscribe and does not provide the opportunity for someone to opt-out. Usage example: GitLab Hosted billing change, Release update 9.0.0 changes, GitLab Page change and Old CI Runner clients.
It is very important to have Engineering and/or Product team (whoever is requesting this type of email) help us narrow these announcements to the people that actually should be warned so we are communicating to a very specific targeted list.

**Newsletter**  
Sent bi-monthly (every 2 weeks). Content Team is responsible for creating the content for each Newsletter.  

**Security Alerts**  
Sent on an as needed basis containing important information about any security patches, identified vulnerabilities, etc related to the GitLab platform. These emails are purely text based.

**Webcasts**   
Invitation and/or notification emails sent about future webcasts.   

**Live Events**   
Invitation emails to attend a live event (VIP or Executive Lunch), meet-up, or in-person training. These emails are sent to a geo-locational subset of the overall segment. This type of email is also used when we are attending a conference and want to make people aware of any booth or event we may be holding and/or sponsoring.


## Website Form Management   

The forms on about.gitlab are embedded Marketo forms. Any changes to the fields, layout, labels and CSS occur within Marketo and can be pushed live without having to make any changes to the source file on GitLab. When needing to change or embed a whole new form, ping Marketing OPS on the related issue so appropriate form and subsequent workflows can be created.   

In the event Marketo has an outage and/or the forms go offline the forms with highest usage/visibility (Free Trial and Contact Us) have been recreated as Google forms that can be embedded on the related pages as a temporary measure to minimize any effect till the outage is past.
