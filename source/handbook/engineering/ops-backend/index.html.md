---
layout: markdown_page
title: "Ops Backend Department"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Teams

There are a number of teams within the Ops Backend group:

* [CI/CD](/handbook/engineering/ops-backend/ci-cd/)
* [Configuration](/handbook/engineering/ops-backend/configuration/)
* [Monitoring](/handbook/engineering/ops-backend/monitoring/)
* [Security Products](/handbook/engineering/ops-backend/security-products/)

Each team has a different focus on what issues to work on for each
release. The following information is not meant to be a set of hard-and-fast
rules, but as a guideline as to what team decides can best improve certain
areas of GitLab.

APIs should be shared responsibility between all teams within the
Backend group.

There is a backend group call every Tuesday, before the team call. You should
have been invited when you joined; if not, ask your team lead!

[Find the product manager mapping to engineering teams in the product handbook](/handbook/product)
