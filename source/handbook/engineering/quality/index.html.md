---
layout: markdown_page
title: "Quality"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Common Links

- [**Public Issue Tracker (for GitLab CE)**](https://gitlab.com/gitlab-org/gitlab-ce);
  please use confidential issues for topics that should only be visible to team members at GitLab.
- Chat channels; please use chat channels for questions that don't seem appropriate to use the issue tracker for.
  - [#quality](https://gitlab.slack.com/messages/quality): for general conversation about GitLab Quality
  - [#triage](https://gitlab.slack.com/messages/triage): general triage team channel.
  - [#mr-coaching](https://gitlab.slack.com/archives/mr-coaching): for general
    conversation about Merge Request coaching.
  - [#opensource](https://gitlab.slack.com/archives/opensource): for general
    conversation about Open Source.

## Quality team goals

### OKRs

See [/okrs/](/okrs/)

### High level long-term goals

* Ensure that GitLab consistently releases high-quality code across the growing suite of products, developing software at scale without sacrificing stability, quality, velocity or code health. 
  * Own and build out our end-to-end automated testing framework and infrastructure
    * Develop our testing harness and tie together build and test pipelines  
    * Develop comprehensive test results tracking and reporting tools that aid in triaging test results
    * Partner with all engineering teams to ensure that our products have testability which we can use from within the test infrastructure
  * Improve [GitLab QA] test coverage, reliability and efficiency
    * Improve on the duration and de-duplication of the GitLab test suites
    * Improve stability by eliminating flaky tests and transient failures
    * Define the best place to test and which tests should run when
    * Coach developers contributing to [GitLab QA] scenarios (both internal and external)
    * How can we put test result data in front of the right people and have them act on it
  * Bake in a culture of quality from within and early in the process
    * We use our knowledge of testing and testability to influence better software design, promote proper engineering practice and bug prevention strategies
    * Integrate feedback from Sales & Customer Support 
  * Stabilize & improve the release process
    * What should we be checking prior to each release, when & who is responsible
    * When and how should automated tests be run
    * Learn from common or frequent mistakes & regressions
  * Make improvement suggestions to the development processes
    * Create a feedback loop from test results to improve test gaps in our test suites
    * Unit test coverage
    * End-to-end test coverage
    * CE/EE maintenance      
* Improve developer efficiency and productivity
  * Gain insight into the development and test metrics to make suggestions for improvement
    * See the [gitlab-insights project](https://gitlab.com/gitlab-org/gitlab-insights)
  * Increase contributors productivity by improving the development setup, workflow, processes, and tools
  * Identifying architectural and [backstage](/job-families/specialist/backstage/) improvements as well as technical debt
* [Triaging issues](/job-families/specialist/issue-triage/) reported by the community 
  * [Issue Triage process and on-boarding](/handbook/quality/issue-triage/)
  
## Test Automation

The GitLab test automation framework is distributed across two projects: 
* [GitLab QA], the test orchestration tool
* The scenarios and spec files within the GitLab main codebase.

### Architecture overview

See the [GitLab QA Documentation](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs).

Our current architecture overview is [here](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs/architecture.md).

### Installation & Execution

* Install and set up the [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit)
* Install and run [GitLab QA] to kick off test execution. See the README for more information on how to run the test framework.
  * The spec files (test cases) can be found in the [GitLab main codebase](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/qa)

## Development process quality

* Merge request checklist
  * The default template is saved at the project level and can be viewed/edited in Settings -> Repository 
  * There are also [specific templates for Database and Documentation changes](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/.gitlab/merge_request_templates)

## Issue Triage

The issue triage process can be found at: [Issue Triage](/handbook/quality/issue-triage/)

## Release process overview

The release process follows [this set of templates](https://gitlab.com/gitlab-org/release-tools/tree/master/templates). Additional supporting docs about the release process can be found [here](https://gitlab.com/gitlab-org/release/docs/).

* First Deployable RC
    * Automated tests are run against Staging and Canary
    * An Issue is created listing the QA tasks for this release candidate
        * The items from the Release Kickoff Doc are copied to the Issue and associated with the appropriate PM
        * The PM performs testing on staging and lists any issues found
        * Other changes related to bugs and improvements are gathered from a Git comparison of this release from the previous one
        * The Engineering teams confirms that these other changes come with tests or have been tested on staging
    * After 24 hours, assuming there are no blocking bugs found, the expectation is testing has completed and the release can proceed to deploy to production
    * Example: [10.6 RC1 QA Task](https://gitlab.com/gitlab-org/release/tasks/issues/116)
* Subsequent RCs
    * Automated tests are run against Staging and Canary
    * An issue is created listing the QA tasks for this release candidate
        * The changes that had been merged in since the previous release are captured in a QA task Issue, associated with the appropriate engineer
        * The engineer performs testing on staging, or confirms that the change came with automated tests that are passing
    * After 12 hours, assuming there are no blocking bugs found, the expectation is testing has completed and the release can proceed to deploy to production
    * Example: [10.6 RC6 QA Task](https://gitlab.com/gitlab-org/release/tasks/issues/135)
* Final Release
    * Automated tests are run against Staging and Canary as a sanity check
    * Since no changes should have been included between the last RC and the release-day build, no additional testing or review should be required.
    * The final release candidate is deployed to production

## Recruiting
* Information regarding the technical quizzes and assignments that are sent to candidates can be found [here](https://gitlab.com/gitlab-com/people-ops/applicant-questionnaires/blob/master/automation-engineer.md) (GITLAB ONLY)
* The steps of the hiring process can be found [here](https://gitlab.com/gitlab-com/people-ops/hiring-processes/blob/master/engineering.md) (GITLAB ONLY)

## Other Related Pages

- [Engineering](/handbook/engineering)
- [Issue Triage](issue-triage)
- [Issue Triage Policies](/handbook/engineering/issue-triage)
- [Performance of GitLab](/handbook/engineering/performance)
- [Monitoring of GitLab.com](/handbook/infrastructure/monitoring)
- [Production Readiness Guide](https://gitlab.com/gitlab-com/infrastructure/blob/master/.gitlab/issue_templates/production_readiness.md)

[GitLab QA]: https://gitlab.com/gitlab-org/gitlab-qa
